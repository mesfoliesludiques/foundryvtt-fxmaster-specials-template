export const Effects = {
    label: "Specials Template",
    effects: [
      {
        label: "Blood",
        file: "modules/specials-template/assets/blood.webm",
        scale: {
          x: 1.0,
          y: 1.0,
        },
        angle: 0,
        anchor: {
          x: 0.5,
          y: 0.5,
        },
        speed: 0,
        author: "U~man",
      },
    ],
  };
  