## FoundryVTT FXMaster Specials Demo Template
This is a demo module you can use to add your own special effects to FXMaster. Here are the important things to remember.

- The `loadEffects.js` file should be loaded in your `module.json` file.
- The `effects.js` should be edited to add and configure your own effects

In order to generate your effects you can use FXMaster specials creation workflow.
Then you can grab your custom effects by using the following command in the web developper console (pressing F12)

```javascript
copy(CONFIG.fxmaster.specials.custom.effects)
```

The effects will be copied in your Clipboard, pasting it in `effects.js` effects array should work just fine.
